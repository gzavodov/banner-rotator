module gitlab.com/gzavodov/banner-rotator

go 1.13

require (
	github.com/766b/go-outliner v0.0.0-20180511142203-fc6edecdadd7 // indirect
	github.com/cucumber/godog v0.8.1
	github.com/gzavodov/otus-go/banner-rotation v0.0.0-20200226222227-4d695414bfc2
	github.com/jackc/pgx/v4 v4.5.0
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	go.uber.org/multierr v1.5.0 // indirect
	go.uber.org/zap v1.14.0
	golang.org/x/crypto v0.0.0-20200311171314-f7b00557c8c4 // indirect
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e
	golang.org/x/tools v0.0.0-20200311184636-0d653b92c519 // indirect
	honnef.co/go/tools v0.0.1-2020.1.3 // indirect
)
